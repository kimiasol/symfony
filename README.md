# Symfony Technical Test Kimia
*Symfony technical test for Kimia*  
Documentation and test requirements at [REQUISITES_SymfonyTechnicalTestKimia.pdf](https://github.com/abraham-kimia/symfony-technical-test/blob/main/doc/REQUISITES_SymfonyTechnicalTestKimia.pdf).

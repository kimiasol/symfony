<?php

namespace App\Controller;

use App\Form\GameType;
use App\Repository\PlayerRepository;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AnnotationLoader;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

/**
 * @Route("/game")
 */
class GameController extends AbstractController
{
    /**
     * @Route("/", name="game_index", methods={"GET"})
     */
    public function index(): Response
    {
        //TODO IN TECHNICAL TEST (endpoint logic)
    }

    /**
     * @Route("/ajax-random-players", name="game_ajax_random_players", methods={"GET"})
     */
    public function ajaxRandomPlayers(PlayerRepository $playerRepository): Response
    {
        //TODO IN TECHNICAL TEST (endpoint logic)
    }
}